package io.bekey.holding;

import com.codeborne.selenide.Configuration;
import io.bekey.holding.model.HoldingFormInterface;
import io.bekey.holding.step.RostestUral;
import org.testng.annotations.*;

import static com.codeborne.selenide.Configuration.*;
import static com.codeborne.selenide.Selenide.open;

public class HoldingFormTest {

    @BeforeMethod
    public void before() {
        browser = "chrome";
        browserSize = "1920x1080";
        screenshots = false;
        //timeout = 15000;
    }

    @AfterMethod
    public void after() {

    }

    @Test
    public void rostestUralTest() {
        RostestUral site = new RostestUral();
        site.openForm();
        site.checkForm();
    }
}
