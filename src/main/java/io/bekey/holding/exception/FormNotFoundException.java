package io.bekey.holding.exception;

public class FormNotFoundException extends RuntimeException {

    public FormNotFoundException() {
        super();
    }

    public FormNotFoundException(String text) {
        super(text);
    }
}
