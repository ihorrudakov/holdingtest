package io.bekey.holding.step;

import io.bekey.holding.exception.FormNotFoundException;

import static com.codeborne.selenide.Selectors.*;
import static com.codeborne.selenide.Selenide.*;

public class RostestUral extends BaseSteps {
    @Override
    public void openForm() {
        open("http://rostestural.ru/");
        /*boolean res = $(byXpath("//div[contains(@class, 'expecto-close-btn expecto-catcher-form-wrapper')]/img")).getAttribute("style").equals("display:none");
        if (res) {
            $(byXpath("//div[contains(@class, 'expecto-close-btn expecto-catcher-form-wrapper')]")).click();
        }
        if (!$(byXpath("//div[@class='moclients-widget__close']/img")).getAttribute("style").equals("")) {
            click("//div[@class='moclients-widget__close']");
        }*/
        click("//a[@id='zakaz-sertificata']");
    }

    @Override
    public void checkForm() {
        sleep(3000);
        boolean form = $(byXpath("//div[@class='form-vopros'][contains(.,'Заказать онлайн')]")).isDisplayed();
        if(!form) {
            throw new FormNotFoundException("Форма на ростестурал не открывается");
        }
    }
}
