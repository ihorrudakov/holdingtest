package io.bekey.holding.step;

import com.codeborne.selenide.ex.ElementNotFound;
import io.bekey.holding.model.HoldingFormInterface;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.sleep;

public abstract class BaseSteps implements HoldingFormInterface {
    public void click(String xpath) {
        try {
            sleep(1000);
            $(byXpath(xpath)).shouldBe(visible).click();
        } catch (ElementNotFound e) {
            System.out.println("error");
        }
    }
}
