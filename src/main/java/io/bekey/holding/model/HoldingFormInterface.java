package io.bekey.holding.model;

public interface HoldingFormInterface {

    public void openForm();

    public void checkForm();
}
